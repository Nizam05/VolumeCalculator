<?php
namespace Pondit\Calculator\VolumeCalculator;


class Cube
{
    public $side_length;

    public function getSL()
    {
        return $this->side_length * $this->side_length * $this->side_length;
    }
}