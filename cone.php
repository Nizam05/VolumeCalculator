<?php
/**
 * Created by PhpStorm.
 * User: toshiba
 * Date: 3/16/2018
 * Time: 12:37 PM
 */


include_once "vendor/autoload.php";

use Pondit\Calculator\VolumeCalculator\Cone;
use Pondit\Calculator\VolumeCalculator\Displayer;

$cone1 = new Cone();
$cone1->pi = 3.14159;
$cone1->height = 6;
$cone1->radius=4;



$displayer=new Displayer();
$displayer->displaypre($cone1->getCon());
$displayer->displayH1($cone1->getCon());
$displayer->displaysimple($cone1->getCon());

